<?php
    if (isset($_GET['url'])) {
        $siteURL = urlencode($_GET['url']);

        $googlePagespeedData = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=$siteURL&screenshot=true&key=AIzaSyBw4UXt34osBTaVCkxURHMbwxZU2eRXjHA");

        //decode json data
        $googlePagespeedData = json_decode($googlePagespeedData, true);

        //screenshot data
        $screenshot = $googlePagespeedData['screenshot']['data'];
        $screenshot = str_replace(array('_','-'),array('/','+'),$screenshot); 

        echo "data:image/jpeg;base64,".$screenshot;
    }
?>